DEBUG	?= 1

CC		?= gcc
RM		?= rm
INSTALL_PATH	?= /usr/local

OUTPUT	?= wonderfence

OBJECTS	?=
OBJECTS	+= main.o ini.o
OBJECTS	+= wonfence.o wonfence_impl.o
OBJECTS	+= wongate.o wongate_ser.o wongate_cmd.o wongate_hack.o
OBJECTS	+= tser/tser.o tser/tser_err.o tser/tser_ser.o

OBJECTS	:= $(addprefix obj/,$(OBJECTS))

CFLAGS	?= -Wall -Wextra -pedantic -Wno-unused-label -Wno-unused-parameter -Wno-variadic-macros
LDFLAGS	?=

LDFLAGS	+= -lev
ifeq ($(DEBUG),1)
CFLAGS	+= -g -DDEBUG=1
LDFLAGS	+= -g
else
CFLAGS	+= -O2 -DDEBUG=0
endif

.PHONY: all clean install uninstall

all: $(OBJECTS) $(OUTPUT)
obj/%.o: src/%.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $+
clean:
	$(RM) -f $(OUTPUT) $(OBJECTS)
install:
	install	$(OUTPUT) $(INSTALL_PATH)/bin
uninstall:
	install	/dev/null $(INSTALL_PATH)/bin/$(OUTPUT)
