#ifndef WONGATE_SER_H_
#define WONGATE_SER_H_

tser_errcode wongate_ser_recv(tser_t* ts, wongate_cmd_t* cmd); /* Get command from serial */
tser_errcode wongate_ser_send(tser_t* ts, wongate_cmd_t* cmd); /* Send command to serial */

#endif
