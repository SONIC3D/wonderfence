#include "top.h"

wongate_cmd_t* wongate_cmd_new(void)
{
  wongate_cmd_t* cmd;

  cmd = malloc(sizeof(wongate_cmd_t));
  assert(cmd != NULL);

  cmd->type = 0;
  cmd->zero = 0;
  cmd->size = 0;
  cmd->cmd = 0;
  cmd->param = NULL;

  return cmd;
}
void wongate_cmd_free(wongate_cmd_t* cmd)
{
  if(cmd == NULL)
    return;

  if(cmd->param != NULL) {
    free(cmd->param);
    cmd->param = NULL;
  }

  free(cmd);
}
int wongate_cmd_adjust(wongate_cmd_t* cmd)
{
  int err = -1;

  if(cmd->param != NULL) {
    free(cmd->param);
    cmd->param = NULL;
  }

  cmd->param = malloc(cmd->size);
  assert(cmd->param != NULL);

  err = 0;
_fail:
  return err;
}

void wongate_cmd_setparam_byte(wongate_cmd_t* cmd, WORD adr, BYTE val)
{
  cmd->param[adr] = val;
}
void wongate_cmd_setparam_word(wongate_cmd_t* cmd, WORD adr, WORD val)
{
  wongate_cmd_setparam_byte(cmd, adr+0, (val >> 0) & 0xFF);
  wongate_cmd_setparam_byte(cmd, adr+1, (val >> 8) & 0xFF);
}

BYTE wongate_cmd_getparam_byte(wongate_cmd_t* cmd, WORD adr)
{
  return cmd->param[adr];
}
WORD wongate_cmd_getparam_word(wongate_cmd_t* cmd, WORD adr)
{
  return (wongate_cmd_getparam_byte(cmd, adr+0) << 0) |
         (wongate_cmd_getparam_byte(cmd, adr+1) << 8);
}
