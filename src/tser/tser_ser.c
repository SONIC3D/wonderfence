#include "tser.h"

/* Get serial bytes (blocking) */
tser_errcode tser_recv(tser_t* ts, size_t len, uint8_t* buf)
{
  tser_errcode err = TSER_ERR_UNK;
  size_t i;

  for(i = 0; i < len; i++) {
    err = tser_recv_byte(ts, &buf[i]);
    if(err < 0) goto _fail;
  }

  err = TSER_ERR_NONE;
_fail:
  return err;
}

/* Send serial bytes (blocking) */
tser_errcode tser_send(tser_t* ts, size_t len, uint8_t* buf)
{
  tser_errcode err = TSER_ERR_UNK;
  size_t i;

  for(i = 0; i < len; i++) {
    err = tser_send_byte(ts, buf[i]);
    if(err < 0) goto _fail;
  }

  err = TSER_ERR_NONE;
_fail:
  return err;
}

/* Get serial byte (blocking) */
tser_errcode tser_recv_byte(tser_t* ts, uint8_t* c)
{
  tser_errcode err = TSER_ERR_UNK;
  int ret;
  uint8_t ic;

  do {
    ret = read(ts->fds[0], &ic, 1);
    if(ret == 0) /* Do some waiting so we don't go nuts on CPU */
      usleep(50);
  } while(ret == 0);

  if(ret < 0) {
    err = TSER_ERR_SERPORT_RX;
    goto _fail;
  }

  *c = ic;

  err = TSER_ERR_NONE;
_fail:
  return err;
}

/* Send serial byte (blocking) */
tser_errcode tser_send_byte(tser_t* ts, uint8_t c)
{
  tser_errcode err = TSER_ERR_UNK;
  int ret;

  do {
    ret = write(ts->fds[1], &c, 1);
    if(ret == 0) /* Do some waiting so we don't go nuts on CPU */
      usleep(50);
  } while(ret == 0);

  if(ret < 0) {
    err = TSER_ERR_SERPORT_TX;
    goto _fail;
  }

  err = TSER_ERR_NONE;
_fail:
  return err;
}
