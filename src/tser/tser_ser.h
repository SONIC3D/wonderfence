#ifndef TSER_SER_H_
#define TSER_SER_H_

/* Get serial bytes (blocking) */
tser_errcode tser_recv(tser_t* ts, size_t len, uint8_t* buf);
/* Send serial bytes (blocking) */
tser_errcode tser_send(tser_t* ts, size_t len, uint8_t* buf);
/* Get serial byte (blocking) */
tser_errcode tser_recv_byte(tser_t* ts, uint8_t* c);
/* Send serial byte (blocking) */
tser_errcode tser_send_byte(tser_t* ts, uint8_t c);

#endif
