#ifndef TSER_ERR_H_
#define TSER_ERR_H_

enum tser_errcode {
  TSER_ERR_NONE = 0,

  TSER_ERR_UNK = -0x8000,
  TSER_ERR_SERPORT_RX,
  TSER_ERR_SERPORT_TX,
  TSER_ERR_SERPORT_CHECK,
};
typedef enum tser_errcode tser_errcode;

void tser_perror(tser_t* ts, tser_errcode err);

#endif
