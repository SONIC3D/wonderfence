#ifndef WONGATE_H_
#define WONGATE_H_

#define FIRST_SOCK (0x30)
/* Ver 1.0 */
#define WONDERGATE_VERSION (0x10)

typedef struct wongate_t wongate_t;
typedef struct wongate_cfg_t wongate_cfg_t;
typedef struct wongate_hosthack_t wongate_hosthack_t;
typedef struct wongate_sockhack_t wongate_sockhack_t;

/* WonderGate status */
enum {
  MWGSTAT_OFF = 0, /* ? */
  MWGSTAT_ON  = 2,
  MWGSTAT_PPP = 3,
};

enum {
  PDCSTAT_OK = 0,
  PDCSTAT_BUSY = 1,
  PDCSTAT_NOSVC = 2,
  PDCSTAT_NOPDC = 3,
};

enum {
  PPPSTAT_BAD = 0,
  PPPSTAT_OK = 1,
};

enum {
  DIALSTAT_OK = 0,
  DIALSTAT_NORESP = 1,
  DIALSTAT_BUSY = 2,
  DIALSTAT_REDIAL = 3,
  DIALSTAT_NOCOM = 0xF0,
  DIALSTAT_NOPDC = 0xFF,
};

/* WonderGate */
struct wongate_t {
  BYTE ver;
  BYTE status;

  uint32_t dns1, dns2;

  /* PDC state */
  struct {
    BYTE reception;
    BYTE status;
  } pdc;

  /* PPP state */
  struct {
    char* user;
    char* pass;
  } ppp;

  /* Phone state */
  struct {
    char* dialed; /* Currently dialed number */
  } phone;

  /* Sockets state */
  struct {
    int mapping[255];
  } sockets;

  wongate_cfg_t* cfg; /* Configuration */
};

/* WonderGate configuration */
struct wongate_cfg_t {
  struct {
    int reception;
  } pdc;
  struct {
    char* user;
    char* pass;
  } ppp;

  int hosthack_count;
  wongate_hosthack_t* hosthacks;

  int sockhack_count;
  wongate_sockhack_t* sockhacks;
};

/* WonderFence hostname hack information */
struct wongate_hosthack_t {
  char* in_name;
  char* out_name;
};

/* WonderFence socket hack information */
struct wongate_sockhack_t {
  /* All in network order */
  uint32_t in_ip;
  uint16_t in_port; /* 0 denotes no port */
  uint32_t out_ip;
  uint16_t out_port; /* 0 denotes no port */
};


wongate_t* wongate_new(void); /* Create new wondergate */
void wongate_free(wongate_t* gate); /* Release wondergate */
void wongate_clear(wongate_t* gate);
void wongate_set_config(wongate_t* gate, wongate_cfg_t* cfg);

WORD wongate_poweron(wongate_t* gate);
BYTE wongate_poweroff(wongate_t* gate);

BYTE wongate_status(wongate_t* gate);
BYTE wongate_pdc_status(wongate_t* gate);
BYTE wongate_pdc_reception(wongate_t* gate);

BYTE wongate_set_ppp_login(wongate_t* gate, char* user, char* pass);
BYTE wongate_set_dns(wongate_t* gate, BYTE dns1[4], BYTE dns2[4]);

BYTE wongate_dial(wongate_t* gate, char* num);
BYTE wongate_hangup(wongate_t* gate);

BYTE wongate_gethost(wongate_t* gate, char* name, char** newname, uint8_t* ipadr);
BYTE wongate_socknew(wongate_t* gate);
BYTE wongate_sockcon(wongate_t* gate, BYTE s, WORD afam, BYTE* data, BYTE datalen);
BYTE wongate_sockdel(wongate_t* gate, BYTE s);
BYTE wongate_socksend(wongate_t* gate, BYTE s, BYTE* data, BYTE datalen);
BYTE wongate_sockrecv(wongate_t* gate, BYTE s, BYTE* data, BYTE datalen);

#endif
