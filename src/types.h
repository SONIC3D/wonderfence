#ifndef TYPES_H_
#define TYPES_H_

typedef uint8_t  BYTE;
typedef uint16_t WORD;
typedef int8_t   SBYTE;
typedef int16_t  SWORD;

#endif
