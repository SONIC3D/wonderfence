#ifndef TOP_H_
#define TOP_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#include "tser/tser.h"

#include "types.h"

#include "wongate.h"
#include "wongate_cmd.h"
#include "wongate_hack.h"
#include "wongate_ser.h"

#include "wonfence.h"

#if DEBUG
# define dbgprintf(args...) fprintf(stderr, args)
#else
# define dbgprintf(args...)
#endif

#endif
