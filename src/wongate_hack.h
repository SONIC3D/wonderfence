#ifndef WONGATE_HACK_H_
#define WONGATE_HACK_H_

void wongate_HACK_hostname(wongate_t* gate, char* name, char** rname);
void wongate_HACK_socket(wongate_t* gate, uint32_t* addr, uint16_t* port);

#endif
