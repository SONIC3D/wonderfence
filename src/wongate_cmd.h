#ifndef WONGATE_CMD_H_
#define WONGATE_CMD_H_

typedef struct wongate_cmd_t wongate_cmd_t;

/* WonderGate command */
struct wongate_cmd_t {
  BYTE type;
  BYTE zero;
  BYTE size;
  BYTE cmd;

  BYTE* param;
};

wongate_cmd_t* wongate_cmd_new(void); /* Create new command */
void wongate_cmd_free(wongate_cmd_t* cmd); /* Release command */
int wongate_cmd_adjust(wongate_cmd_t* cmd); /* Setup 'param' after header is set */

void wongate_cmd_setparam_byte(wongate_cmd_t* cmd, WORD adr, BYTE val);
void wongate_cmd_setparam_word(wongate_cmd_t* cmd, WORD adr, WORD val);

BYTE wongate_cmd_getparam_byte(wongate_cmd_t* cmd, WORD adr);
WORD wongate_cmd_getparam_word(wongate_cmd_t* cmd, WORD adr);

#endif
