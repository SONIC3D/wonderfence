#ifndef WONFENCE_H_
#define WONFENCE_H_

typedef struct wonfence_handler_t wonfence_handler_t;

typedef struct wonfence_t wonfence_t;

#define WONFENCE_HANDLER_ARGS wonfence_t* fence,wongate_t* gate, wongate_cmd_t* cmd, wongate_cmd_t* repl
#define WONFENCE_HANDLER(HND) int wonfence_handler_##HND(WONFENCE_HANDLER_ARGS)

typedef int (*fence_cmdhnd)(WONFENCE_HANDLER_ARGS);

/* WonderFence state */
struct wonfence_t {
  wongate_t* gate;
  wongate_cfg_t cfg;
};

wonfence_t* wonfence_new(void);
void wonfence_free(wonfence_t* fence);
void wonfence_clear(wonfence_t* fence);

int wonfence_load_config(wonfence_t* fence, const char* fn);

/* WonderFence handler */
struct wonfence_handler_t {
  WORD type; /* -1 for sentinel */
  WORD cmd; /* -1 for any */
  fence_cmdhnd hndlr;
  char* name;
};

int wonfence_handle_cmd(wonfence_t* fence, wongate_cmd_t* cmd, wongate_cmd_t** repl);

#endif
