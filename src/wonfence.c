#include "top.h"
#include "ini.h"
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int xtoi(const char* str);
int xtoi_bs(const char* str, int base);
int xtoi_full(const char* str, int base, const char** sstr);

#define WONFENCE_CMD(TYPE,CMD, HND) WONFENCE_HANDLER(HND);
#include "wonfence_cmdlist.h"
#undef WONFENCE_CMD

static const wonfence_handler_t fence_handlers[] = {
#define WONFENCE_CMD(TYPE,CMD, HND) { TYPE,CMD, wonfence_handler_##HND, #HND },
#include "wonfence_cmdlist.h"
#undef WONFENCE_CMD
};

static int wonfence_dispatch_handler(wonfence_t* fence, const wonfence_handler_t* hnd, wongate_cmd_t* cmd, wongate_cmd_t** orepl)
{
  int err = -1;
  wongate_cmd_t* repl;

  repl = wongate_cmd_new();
  repl->type = cmd->type;
  repl->zero = 0;
  repl->cmd = cmd->cmd;

  dbgprintf("Handler: %s\n", hnd->name);

  err = hnd->hndlr(fence,fence->gate, cmd, repl);
  if(err < 0) goto _fail;

  if((repl->zero & 0x80) == 0) {
    repl->zero = 0;
    *orepl = repl;
  }

  err = 0;
_fail:
  return err;
}

int wonfence_handle_cmd(wonfence_t* fence, wongate_cmd_t* cmd, wongate_cmd_t** repl)
{
  int err = -1;
  const wonfence_handler_t* hnd;

  *repl = NULL;
  for(hnd = fence_handlers; hnd->type != 0xFFFF; hnd++) {
    if(hnd->type != cmd->type)
      continue;

    if((hnd->cmd != 0xFFFF) && (hnd->cmd != cmd->cmd))
      continue;

    break;
  }
  err = wonfence_dispatch_handler(fence, hnd, cmd, repl);
  if(err < 0) goto _fail;

  err = 0;
_fail:
  return err;
}

wonfence_t* wonfence_new(void)
{
  wonfence_t* fence;

  fence = malloc(sizeof(wonfence_t));
  assert(fence != NULL);

  fence->gate = wongate_new();

  fence->cfg.ppp.user = NULL;
  fence->cfg.ppp.pass = NULL;

  fence->cfg.hosthacks = NULL;

  fence->cfg.sockhacks = NULL;

  wonfence_clear(fence);

  return fence;
}
void wonfence_free(wonfence_t* fence)
{
  if(fence == NULL)
    return;

  if(fence->gate != NULL) {
    wongate_free(fence->gate);
    fence->gate = NULL;
  }

  free(fence);
}
#define VAR_UNSETPTR(VAR) do { \
    if(VAR != NULL) { \
      free(VAR); \
      VAR = NULL; \
    } \
} while(0)

void wonfence_clear(wonfence_t* fence)
{
  int i;
  wongate_clear(fence->gate);

  fence->cfg.pdc.reception = 15;

  VAR_UNSETPTR(fence->cfg.ppp.user);
  VAR_UNSETPTR(fence->cfg.ppp.pass);

  for(i = 0; i < fence->cfg.hosthack_count; i++) {
    VAR_UNSETPTR(fence->cfg.hosthacks[i].in_name);
    VAR_UNSETPTR(fence->cfg.hosthacks[i].out_name);
  }
  fence->cfg.hosthack_count = 0;
  VAR_UNSETPTR(fence->cfg.hosthacks);

  fence->cfg.sockhack_count = 0;
  VAR_UNSETPTR(fence->cfg.sockhacks);
}

#define CFG_SETSTR(VAR,VAL) do { \
    VAR_UNSETPTR(VAR); \
    VAR = strdup(VAL); \
} while(0)

static int wonfence_cfg_handle_pdc(wonfence_t* fence, const char* name, const char* value)
{
  int err = 1;

  if(strcmp(name, "reception") == 0) {
    fence->cfg.pdc.reception = xtoi(value);
  }else{
    err = 0;
  }

_done:
  return err;
}

static int wonfence_cfg_handle_ppp(wonfence_t* fence, const char* name, const char* value)
{
  int err = 1;

  if(strcmp(name, "user") == 0) {
    CFG_SETSTR(fence->cfg.ppp.user, value);
  }else if(strcmp(name, "pass") == 0) {
    CFG_SETSTR(fence->cfg.ppp.pass, value);
  }else{
    err = 0;
  }

_done:
  return err;
}

static int wonfence_cfg_handle_hosthack(wonfence_t* fence, const char* name, const char* value)
{
  int err = 1;
  wongate_hosthack_t* hack;

  fence->cfg.hosthack_count++;
  fence->cfg.hosthacks = realloc(fence->cfg.hosthacks, fence->cfg.hosthack_count * sizeof(wongate_hosthack_t));
  hack = &fence->cfg.hosthacks[fence->cfg.hosthack_count-1];

  hack->in_name = strdup(name);
  hack->out_name = strdup(value);

  fprintf(stderr, "Added hostname hack %s -> %s.\n", hack->in_name, hack->out_name);

_done:
  return err;
}

static int _sockhack_get_ipport(const char* str, uint32_t* ip, uint16_t* port)
{
  char* dup;
  char* portptr;
  struct in_addr addr;

  dup = strdup(str);
  portptr = dup;
  while((*portptr != '\0') && (*portptr != ':')) portptr++;
  if(*portptr == '\0') {
    *port = 0;
  }else{
    *portptr = '\0';
    *port = htons(xtoi(portptr+1));
  }
  if(inet_aton(dup, &addr) == 0) {
    fprintf(stderr, "Bad IP address in sockhack %s\n", str);
    free(dup);
    return -1;
  }
  *ip = addr.s_addr;
  free(dup);

  return 0;
}

static int wonfence_cfg_handle_sockhack(wonfence_t* fence, const char* name, const char* value)
{
  int err = 1;
  wongate_sockhack_t* hack;

  fence->cfg.sockhack_count++;
  fence->cfg.sockhacks = realloc(fence->cfg.sockhacks, fence->cfg.sockhack_count * sizeof(wongate_sockhack_t));
  hack = &fence->cfg.sockhacks[fence->cfg.sockhack_count-1];

  if(_sockhack_get_ipport(name, &hack->in_ip, &hack->in_port) < 0) {
    err = 0;
    goto _done;
  }
  if(_sockhack_get_ipport(value, &hack->out_ip, &hack->out_port) < 0) {
    err = 0;
    goto _done;
  }
  fprintf(stderr, "Added socket hack %08X:%04X -> %08X:%04X.\n", hack->in_ip,hack->in_port, hack->out_ip,hack->out_port);

_done:
  return err;
}

static int wonfence_cfg_handle(void* user, const char* section, const char* name, const char* value)
{
  int err = 1;
  wonfence_t* fence = user;

  if(strcmp(section, "pdc") == 0) {
    err = wonfence_cfg_handle_pdc(fence, name, value);
  }else if(strcmp(section, "ppp") == 0) {
    err = wonfence_cfg_handle_ppp(fence, name, value);
  }else if(strcmp(section, "hostname_hack") == 0) {
    err = wonfence_cfg_handle_hosthack(fence, name, value);
  }else if(strcmp(section, "socket_hack") == 0) {
    err = wonfence_cfg_handle_sockhack(fence, name, value);
  }else{
    err = 0;
  }

_done:
  return err;
}

int wonfence_load_config(wonfence_t* fence, const char* fn)
{
  int err = -1;

  wonfence_clear(fence);

  if(fn != NULL) {
    if(ini_parse(fn, wonfence_cfg_handle, fence) < 0) {
      fprintf(stderr, "Error parsing configuration.\n");
      goto _fail;
    }
  }

  wongate_set_config(fence->gate, &fence->cfg);

  err = 0;
_fail:
  return err;
}

int xtoi_full(const char* str, int base, const char** sstr)
{
  int val = 0;
  int done, hit;
  char c;
  const char* ostr;

  ostr = str;
  if(base == -1) base = 10;

  switch(*str) {
    case '0':
      str++;
      switch(*str) {
        case 'x':
          base = 16;
          str++;
          break;
        case 'b':
          base = 2;
          str++;
          break;
        case 'o':
          base = 8;
          str++;
          break;
        default:
          str--;
          break;
      }
      break;
    case '$':
      base = 16;
      str++;
      break;
  }

  hit = 0;
  done = 0;
  while(!done) {
    c = *str++;
    switch(c) {
      case 'a': case 'b':
      case 'c': case 'd':
      case 'e': case 'f':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'a') + 0xA;
        break;
      case 'A': case 'B':
      case 'C': case 'D':
      case 'E': case 'F':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'A') + 0xA;
        break;
      case '8': case '9':
        if(base < 10) {
          done = 1;
          break;
        }
        /* fall thru */
      case '2': case '3':
      case '4': case '5':
      case '6': case '7':
        if(base < 8) {
          done = 1;
          break;
        }
        /* fall thru */
      case '0': case '1':
        val *= base;
        val += c - '0';
        break;
      default:
        done = 1;
        break;
    }
    if(!done) hit = 1;
  }

  if(!hit) {
    str = ostr+1;
    val = 0;
  }

  if(sstr) *sstr = str-1;
  return val;
}

int xtoi_bs(const char* str, int base) { return xtoi_full(str, base, NULL); }
int xtoi(const char* str) { return xtoi_full(str, -1, NULL); }
